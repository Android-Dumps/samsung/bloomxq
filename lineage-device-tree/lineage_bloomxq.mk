#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from bloomxq device
$(call inherit-product, device/samsung/bloomxq/device.mk)

PRODUCT_DEVICE := bloomxq
PRODUCT_NAME := lineage_bloomxq
PRODUCT_BRAND := samsung
PRODUCT_MODEL := SM-F707U1
PRODUCT_MANUFACTURER := samsung

PRODUCT_GMS_CLIENTID_BASE := android-samsung-ss

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="bloomxquex-user 13 TP1A.220624.014 F707U1UES3HWC1 release-keys"

BUILD_FINGERPRINT := samsung/bloomxquex/bloomxq:11/RP1A.200720.012/F707U1UES3HWC1:user/release-keys
