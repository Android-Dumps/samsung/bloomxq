#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_bloomxq.mk

COMMON_LUNCH_CHOICES := \
    lineage_bloomxq-user \
    lineage_bloomxq-userdebug \
    lineage_bloomxq-eng
